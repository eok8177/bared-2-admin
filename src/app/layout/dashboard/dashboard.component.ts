import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';

import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

import { DashboardService } from '../../_services';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    animations: [routerTransition()]
})
export class DashboardComponent implements OnInit {
    public items: Array<any> = [];

    constructor(private dashboardService: DashboardService) {

    }

    ngOnInit() {
        this.dashboardService.items().pipe(first()).subscribe(items => {
            this.items = items.result;
        });
    }
}
