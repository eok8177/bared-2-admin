﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';

@Injectable({ providedIn: 'root' })
export class DashboardService {
    constructor(private http: HttpClient) { }

    apiUrl = environment.apiUrl;

    items() {
        return this.http.get<any>(this.apiUrl+'dashboard');
    }
}